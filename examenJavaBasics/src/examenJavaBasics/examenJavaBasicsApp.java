/**
 * 
 */
package examenJavaBasics;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.List;

import dto.Bike;
import dto.Car;
import dto.Wheel;

/**
 * @author Elisabet Sabat�
 *
 */
public class examenJavaBasicsApp {

	/**
	 * @param args
	 * @throws Exception 
	 */
	
	
	public static void main(String[] args) throws Exception {
		
		Scanner teclat = new Scanner(System.in);
		System.out.println("Bones, usuari, li demanarem una s�rie de dades per a registrar el vostre ve�cle");
		
		boolean opcioT = false; int opc = 0;
		while(!opcioT) {
			System.out.println("Desitja una moto o un cotxe? ");
			String opcio = teclat.next();
			if(opcio.equalsIgnoreCase("cotxe")||opcio.equalsIgnoreCase("moto")) {
				opcioT = true;
				if(opcio.equalsIgnoreCase("cotxe"))
					opc = 1;
			}
				
			
		}
		
		System.out.println("Introdueix la matr�cula");
		String matricula = obtenirMatricula(teclat);
		System.out.println("Introdueix la marca:");
		String marca = teclat.next();
		System.out.println("Introdueix el color:");
		String color = teclat.next();
		
		
		if(opc == 1) {
			
			
			Car coche = new Car(matricula, marca, color);
			
			System.out.println("Informaci� sobre el cotxe que just heu introdu�t: \n"+coche);
			
			System.out.println("Introducci� de les rodes traseres: \n -----------------------");
			
			//obtenci� de les rodes per mitj� e� m�tode crearRoda
			Wheel rodaD1 = crearRoda(teclat);
			Wheel rodaD2 = crearRoda(teclat);

			
			//creaci� d'array de les delanteres
			List<Wheel> wheelsD = new ArrayList<Wheel>();
			
			//introduim els objectes creats
			wheelsD.add(rodaD1);
			wheelsD.add(rodaD2);
			

			Wheel rodaT1 = crearRoda(teclat);
			Wheel rodaT2 = crearRoda(teclat);

			List<Wheel> wheelsT = new ArrayList<Wheel>();
			
			wheelsT.add(rodaT1);
			wheelsT.add(rodaT2);
			
			//introducci� de les rodes per mitj� del m�tode de la classe car 'addWheels'
			coche.addWheels(wheelsD, wheelsT);
			
			
		} else {
			
			//En aquest cas creem una moto
			Bike moto = new Bike(matricula, marca, color);
			
			//obtenci� de les rodes per mitj� e� m�tode crearRoda
			Wheel rodaD = crearRoda(teclat);
			Wheel rodaT = crearRoda(teclat);

			
			//creaci� d'array de les delanteres
			List<Wheel> wheels = new ArrayList<Wheel>();
			
			moto.addWheels(wheels);
		}
		
		

		
		
	}

	public static String obtenirMatricula(Scanner teclat) throws Exception{
		boolean matrOK = false; String matricula = "";
		
		//creem un bucle fins que creem una matricula apte
		while(!matrOK) {
		System.out.println("Obtenir matr�cula: ");
		matricula = teclat.next();
		int contador = 0, lletres = 0, numeros = 0;
		
		/* comprobem els diferents caracters introduits per tal de saber
		 * si son lletres o n�meros i les quantitats.
		 */
		while(contador<matricula.length()) {
			
			char caracter = matricula.charAt(contador);
			if(caracter>65&&caracter<122)
				lletres++;
			else if(caracter>48&&caracter<57)
				numeros++;
			
			contador++;
		}
		
		//si es compleixen les condicions donarem la matr�cula per v�lida i sortim del bucle
		if((lletres==3||lletres==4)&&numeros==4)
			matrOK = true;
		}
		
		return matricula;
	}
	
	/*Com que el proc�s de crear per introducci� per teclat �s molt repetitiu
	 * ja que se'ns demana les quatre, he optat per la creaci� d'aquest m�tode
	 * al que he anomenat 'crearRoda' i que retorna un objecte tipo Wheel amb les
	 * dades ja introdu�des. 
	 */
	public static Wheel crearRoda(Scanner teclat) {
		System.out.println("Introdueix la marca de la roda:");
		String marcaRodaD1 = teclat.next();
		
		boolean tamanyRodaCorrecte = false;
		double diametreRodaD1 = 0;
		while(!tamanyRodaCorrecte) {
		System.out.println("Introdueix el di�metre de la roda:");
		//En aquest cas utilitzem el m�tode 'nextDouble' ja que volem obtenir un double.
		 diametreRodaD1 = teclat.nextDouble();
		 //si el tamany introduit est� entre 0.4 i 4 ser� acceptat i sortirem del bucle
		 if(diametreRodaD1>0.4&&diametreRodaD1<4)
			 tamanyRodaCorrecte = true;
		}
		//retornem la creaci� del objecte Wheel amb les dades introduides en aquest m�tode
		return new Wheel(marcaRodaD1, diametreRodaD1);
	}

}
