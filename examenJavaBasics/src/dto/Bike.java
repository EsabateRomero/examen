/**
 * 
 */
package dto;

import java.util.List;

/**
 * @author Elisabet Sabat�
 *
 */
public class Bike extends Vehicle {

	public Bike(String plate, String brand, String color) {
		super(plate, brand, color);
	}
	
	public void addWheels(List<Wheel> Wheels) throws Exception {
		addTwoWheels(Wheels);
	}

	public void addTwoWheels(List<Wheel> wheels) throws Exception {
		if (wheels.size() != 2)
			throw new Exception();

		Wheel rightWheel = wheels.get(0);
		Wheel leftWheel = wheels.get(1);

		this.wheels.add(leftWheel);
		this.wheels.add(rightWheel);
	}

}
