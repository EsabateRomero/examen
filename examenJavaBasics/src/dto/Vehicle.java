/**
 * 
 */
package dto;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Elisabet Sabat�
 *
 */
public abstract class Vehicle {

	protected String plate;
	protected String brand;
	protected String color;
	protected List<Wheel> wheelsd = new ArrayList<Wheel>();
	protected List<Wheel> wheelst = new ArrayList<Wheel>();
	protected List<Wheel> wheels = new ArrayList<Wheel>();

	public Vehicle(String plate, String brand, String color) {
		this.plate = plate;
		this.brand = brand;
		this.color = color;
	}
}

