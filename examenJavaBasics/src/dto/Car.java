/**
 * 
 */
package dto;
import java.util.List;
/**
 * @author Elisabet Sabat�
 *
 */
public class Car extends Vehicle {

	public Car(String plate, String brand, String color) {
		super(plate, brand, color);
	}

	public void addWheels(List<Wheel> frontWheels, List<Wheel> backWheels) throws Exception {
		addTwoWheelsd(frontWheels);
		addTwoWheelst(backWheels);
	}

	public void addTwoWheelsd(List<Wheel> wheels) throws Exception {
		if (wheels.size() != 2)
			throw new Exception();

		Wheel rightWheel = wheels.get(0);
		Wheel leftWheel = wheels.get(1);

		this.wheelsd.add(leftWheel);
		this.wheelsd.add(rightWheel);
	}
	
	public void addTwoWheelst(List<Wheel> wheels) throws Exception {
		if (wheels.size() != 2)
			throw new Exception();

		Wheel rightWheel = wheels.get(0);
		Wheel leftWheel = wheels.get(1);


		this.wheelst.add(leftWheel);
		this.wheelst.add(rightWheel);
	}
	

	
	
	

}
